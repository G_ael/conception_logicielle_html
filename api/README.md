# installer les dépendances
```
pip3 install -r requirements.txt
````

# lancer l'application
```
uvicorn main:app
```
